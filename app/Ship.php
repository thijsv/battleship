<?php

namespace App;

use  Illuminate\Database\Eloquent\Model;

class Ship extends Model
{
    const ORIENTATION_HORIZONTAL = 'horizontal';
    const ORIENTATION_VERTICAL = 'vertical';
}