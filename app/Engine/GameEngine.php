<?php

namespace App\Engine;

use App\Game;
use App\Board;
use App\Ship;

class GameEngine
{
    const MESSAGE_HIT = 'Raak!';
    const MESSAGE_SUNK = 'Schip gezonken: ';
    const MESSAGE_MISS = 'Mis.';
    const MESSAGE_START = 'Begin maar.';
    const MESSAGE_BAD_INPUT = 'Verkeerde coordinaten.';
    const MESSAGE_WINNER = 'Spel gewonnen door: Speler ';
    const MESSAGE_CPU_WINNER = 'Spel gewonnen door de CPU';

    const AI_TYPE_RANDOM = 'random';
    const AI_TYPE_SMARTER = 'smarter';
    const AI_TYPE = GameEngine::AI_TYPE_SMARTER;
    //const AI_TYPE = GameEngine::AI_TYPE_RANDOM;

    const BOARD_HEIGHT = 10;
    const BOARD_WIDTH = 10;
    const SHIP_TYPES = [
        ['name' => 'Vliegdekschip', 'length' => 4, 'width' => 2, 'amount' => 1],
        ['name' => 'Slagschip', 'length' => 4, 'width' => 1, 'amount' => 2],
        ['name' => 'Torpedobootjager', 'length' => 3, 'width' => 1, 'amount' => 2],
        ['name' => 'Patrouilleschip', 'length' => 2, 'width' => 1, 'amount' => 3],
    ];

    private $game;

    public function __construct()
    {
        //set the randomizer seed for random board generation and the "AI"
        srand( time() );

        //try to load an unfinished game, otherwise create a new one
        $this->game = $this->getActiveGame() ?: (new GameInitializer)->createGame();
    }

    public function getGame()
    {
        return $this->game;
    }

    public function startNewGame( $players )
    {
        //finish old game
        $this->game->status = Game::STATUS_FINISHED;
        $this->game->save();

        //start new game
        $this->game = (new GameInitializer)->createGame();
        $this->game->type = $players == 2 ? Game::TYPE_TWO_PLAYERS : Game::TYPE_ONE_PLAYER;
        $this->game->save();
    }

    /*
     * performs a *human* action on the board, and possibly a reaction from the cpu
     */
    public function play($row, $column)
    {
        $board = $this->game->getCurrentPlayerBoard();

        if(!$this->isValidInput($board, $row, $column)) {
            return GameEngine::MESSAGE_BAD_INPUT;
        }

        $message = $this->shoot($board, $row, $column);

        if($this->isWinner($board)) {
            $this->game->finish();
            return $message . ' ' . self::MESSAGE_WINNER . $board->player;
        }

        $this->game->switchPlayer();

        if($this->game->type == Game::TYPE_ONE_PLAYER) {
            $board = $this->game->getCurrentPlayerBoard();

            $shot = $this->selectRandomShot($board);

            $this->shoot($board, $shot['row'], $shot['column']);

            if($this->isWinner($board)) {
                $this->game->finish();
                return self::MESSAGE_CPU_WINNER;
            }

            $this->game->switchPlayer();
        }

        return $message;
    }

    /*
     * plays the game using the AI until one of the players wins
     */
    public function autoplay()
    {
        for($i = 0; $i<10000; $i++) {

            $board = $this->game->getCurrentPlayerBoard();

            $shot = $this->selectRandomShot($board);

            $this->shoot($board, $shot['row'], $shot['column']);

            if($this->isWinner($board)) {
                $this->game->finish();
                return self::MESSAGE_WINNER . $board->player;
            }

            $this->game->switchPlayer();
        }
        return '';
    }

    private function getActiveGame()
    {
        return Game::query()->where('status', Game::STATUS_ACTIVE)->first();
    }

    private function isWinner(Board $board)
    {
        $liveShips = array_filter($board->ships()->get()->all(), function($ship) {
            return $ship->hitpoints > 0;
        });

        return count($liveShips) == 0;
    }

    /*
     * processes a shot at the board, and returns textual feedback based on result
     */
    private function shoot(Board $board, int $row, int $column)
    {
        $cell = $board->getCell($row, $column);

        if($cell !== null) {

            $board->setCell($row, $column, Board::VALUE_HIT);

            $ship = Ship::find( $cell );
            $ship->hitpoints --;
            $ship->save();

            if($ship->hitpoints == 0) {
                $message = GameEngine::MESSAGE_SUNK.$ship->name.'!';
            } else {
                $message = GameEngine::MESSAGE_HIT;
            }
        } else {
            $board->setCell($row, $column, Board::VALUE_MISS);
            $message = GameEngine::MESSAGE_MISS;
        }

        return $message;
    }

    private function selectRandomShot(Board $board)
    {
        $possibleShots = [];

        $maxEval = -1000;
        foreach(range(0, GameEngine::BOARD_HEIGHT-1) as $row) {
            foreach(range(0, GameEngine::BOARD_WIDTH-1) as $column) {
                if($this->isValidShot($board, $row, $column) ) {
                    $evaluation = $this->getShotEvaluation($board, $row, $column);
                    $maxEval = max($evaluation, $maxEval);
                    $possibleShots[] = ['row' => $row, 'column' => $column, 'evaluation' => $evaluation];
                }
            }
        }

        $bestShots = array_values(array_filter($possibleShots, function($shot) use ($maxEval) {
            return $shot['evaluation'] == $maxEval;
        }));

        $pick = rand(0, count($bestShots)-1);
        return $bestShots[$pick];
    }

    //determines the preference the AI will have for certain shots. Higher value means better shot.
    private function getShotEvaluation(Board $board, int $row, int $column)
    {
        if(GameEngine::AI_TYPE == GameEngine::AI_TYPE_RANDOM) {
            //all shots are equal
            return 1;
        }

        /*
         * The idea of this simple AI is to have preference for shots based on previous shots in the direct vicinity.
         * For example, when surrounded by previous misses the chance of hit is low, but when next to a hit, chance of
         * another hit is high.
         */

        $straightNeighbours = [
            $board->getCellSafe($row-1, $column),
            $board->getCellSafe($row+1, $column),
            $board->getCellSafe($row, $column-1),
            $board->getCellSafe($row, $column+1)
        ];

        $diagonalNeighbours = [
            $board->getCellSafe($row-1, $column-1),
            $board->getCellSafe($row-1, $column+1),
            $board->getCellSafe($row+1, $column-1),
            $board->getCellSafe($row+1, $column+1)
        ];

        $eval = 0;

        foreach($straightNeighbours as $cell) {
            if($cell == Board::VALUE_MISS) {
                $eval += -1; //deduct points for being next to a miss
            } else if ($cell == Board::VALUE_HIT) {
                $eval += 5; //add points for being horizontally adjacent to a hit
            }
        }

        foreach($diagonalNeighbours as $cell) {
            if ($cell == Board::VALUE_HIT) {
                $eval += -3; //deduct points for being diagonally adjacent to a hit
            }
        }

        return $eval;
    }

    private function isValidInput(Board $board, int $row, int $column)
    {
        if($row === null || $column === null) {
            return false;
        }
        if(!$board->isValidRow($row) || !$board->isValidColumn($column)) {
            return false;
        }
        return $this->isValidShot($board, $row, $column);
    }

    private function isValidShot(Board $board, int $row, int $column)
    {
        if($board->getCell($row, $column) == Board::VALUE_HIT) {
            return false;
        }
        if($board->getCell($row, $column) == Board::VALUE_MISS) {
            return false;
        }
        return true;
    }
}