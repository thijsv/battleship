<?php

namespace App\Engine;

use App\Game;
use App\Board;
use App\Ship;

class GameInitializer
{
    public function createGame()
    {
        $game = new Game;
        $game->status = Game::STATUS_ACTIVE;
        $game->type = Game::TYPE_ONE_PLAYER;
        $game->current_player = Game::PLAYER_ONE;
        $game->save();

        //create the random boards
        $this->createBoard($game, Game::PLAYER_ONE);
        $this->createBoard($game, Game::PLAYER_TWO);

        return $game;
    }

    private function createBoard(Game $game, int $player)
    {
        $board = new Board;
        $board->game_id = $game->id;
        $board->grid = $this->getEmptyGrid();
        $board->player = $player;
        $board->save();

        foreach(GameEngine::SHIP_TYPES as $shipType) {
            foreach(range(1, $shipType['amount']) as $i) {

                $ship = new Ship;
                $ship->board_id = $board->id;
                $ship->name = $shipType['name'];
                $ship->hitpoints = $shipType['length'] * $shipType['width'];
                $ship->save();

                $this->randomlyPlaceShip($board, $ship->id, $shipType['length'], $shipType['width']);
            }
        }
    }

    private function getEmptyGrid()
    {
        $grid = [];
        foreach(range(0, GameEngine::BOARD_HEIGHT-1) as $row) {
            $grid[$row] = [];
            foreach(range(0, GameEngine::BOARD_WIDTH-1) as $column) {
                $grid[$row][$column] = null;
            }
        }
        return $grid;
    }

    /*
     * tries to place ship at random locations and orientations until success
     */
    private function randomlyPlaceShip(Board $board, int $shipId, $length, $width)
    {
        for($try = 0; $try < 100000; $try++ ) {

            $horizontal = rand(0,1) === 0; //random true or false

            if($horizontal) {
                //place the ship horizontally
                $rowCount = $length;
                $columnCount = $width;
            } else {
                //place the ship vertically
                $rowCount = $width;
                $columnCount = $length;
            }

            $row = rand(0, GameEngine::BOARD_HEIGHT - $rowCount);
            $column = rand(0, GameEngine::BOARD_WIDTH - $columnCount);

            //calculate which cells the ship would occupy
            $rows = range($row, $row+$rowCount-1);
            $columns = range($column, $column+$columnCount-1);

            $result = $this->attemptPlacement($board->grid, $shipId, $rows, $columns);

            //if ship has been succesfully placed, set the updated grid on the board
            if($result) {
                $board->grid = $result;
                $board->save();
                return;
            }
        }

        //if we reach this point, the ship doesn't seem to fit => we just skip it
    }

    /*
     * places the ship on the grid at the given location and orientation
     * returns false if the space on the grid is not fully available
     */
    private function attemptPlacement(array $grid, int $shipId, array $rows, array $columns)
    {
        //place the ship until occupied cell is found
        foreach($rows as $row) {
            foreach($columns as $column) {
                if($grid[$row][$column] !== null) {
                    //occupied cell, location+orientation is not valid
                    return false;
                }
                $grid[$row][$column] = $shipId;
            }
        }

        return $grid;
    }

}