<?php

namespace App;

use  Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    const VALUE_HIT = 'hit';
    const VALUE_MISS = 'miss';

    //casts json in db to array
    protected $casts = [
        'grid' => 'array',
    ];

    public function ships()
    {
        return $this->hasMany(Ship::class);
    }

    public function setCell(int $row, int $column, $value)
    {
        $grid = $this->grid;
        $grid[$row][$column] = $value;
        $this->grid = $grid;
        $this->save();
    }

    public function getCell(int $row, int $column)
    {
        return $this->grid[$row][$column];
    }

    public function isValidRow(int $row)
    {
        return $row >= 0 && $row < \App\Engine\GameEngine::BOARD_HEIGHT;
    }

    public function isValidColumn(int $column)
    {
        return $column >= 0 && $column < \App\Engine\GameEngine::BOARD_WIDTH;
    }

    public function getCellSafe(int $row, int $column)
    {
        if (!$this->isValidRow($row) || !$this->isValidColumn($column)) {
            return null;
        }
        return $this->getCell($row, $column);
    }
}





