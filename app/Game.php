<?php

namespace App;

use  Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    const STATUS_ACTIVE = 'active';
    const STATUS_FINISHED = 'finished';
    const TYPE_ONE_PLAYER = '1p';
    const TYPE_TWO_PLAYERS = '2p';
    const PLAYER_ONE = 1;
    const PLAYER_TWO = 2;

    public function getCurrentPlayerBoard()
    {
        foreach($this->boards as $board) {
            if($board->player == $this->current_player) {
                return $board;
            }
        }
        return null;
    }

    public function boards()
    {
        return $this->hasMany(Board::class);
    }

    public function switchPlayer()
    {
        $this->current_player = $this->current_player == Game::PLAYER_ONE ? Game::PLAYER_TWO : Game::PLAYER_ONE;
        $this->save();
    }

    public function finish()
    {
        $this->status = Game::STATUS_FINISHED;
        $this->save();
    }
}