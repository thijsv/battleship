<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function() {

    $engine = new \App\Engine\GameEngine;

    return view('game', ['game' => $engine->getGame(), 'message' => \App\Engine\GameEngine::MESSAGE_START]);
});

Route::get('new/{players}', function($players) {

    $engine = new \App\Engine\GameEngine;

    $engine->startNewGame($players);

    return view('game', ['game' => $engine->getGame(), 'message' => \App\Engine\GameEngine::MESSAGE_START]);
});

Route::get('play/{row}/{column}', function($row, $column) {

    $engine = new \App\Engine\GameEngine;

    $message = $engine->play($row, $column);

    return view('game', ['game' => $engine->getGame(), 'message' => $message]);
});

Route::get('autoplay', function() {

    $engine = new \App\Engine\GameEngine;

    $message = $engine->autoplay();

    return view('game', ['game' => $engine->getGame(), 'message' => $message]);
});
