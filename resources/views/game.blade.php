<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Battleship!</title>

        <!-- Styles -->
        <style>
            table tr td {
                width: 25px;
                height: 25px;
            }

            table tr td.hit {
                background-color: red;
            }

            table tr td.boat {
                background-color: blue;
            }

            table tr td.miss {
                background-color: gray;
            }

            table tr td.available {
                background-color: white;
            }

            table tr td.empty {
                background-color: white;
            }

            table tr td.available:hover {
                cursor: pointer;
                background-color: blue;
            }

            div {
                margin-top: 20px;
                margin-bottom: 20px;
            }

            .message {
                font-weight: 600;
            }

            .passive {
                opacity: 0.5;
            }

        </style>
    </head>
    <body>

        <div class="message">{{$message}}</div>

        @foreach($game->boards as $board)
            <div class="{{$board->player == $game->current_player ? 'active' : 'passive'}}">
                <span>
                    Speler {{$board->player}}
                    {{$board->player == \App\Game::PLAYER_TWO && $game->type == \App\Game::TYPE_ONE_PLAYER ? " (CPU)" : ""}}
                </span>
                <table border="1">
                    @foreach($board->grid as $row => $rowData)
                        <tr>
                            @foreach($rowData as $column => $cell)
                                @if($board->player == $game->current_player)
                                    @if($cell == \App\Board::VALUE_HIT)
                                        <td class="hit"></td>
                                    @elseif($cell == \App\Board::VALUE_MISS)
                                        <td class="miss"></td>
                                    @elseif($game->status == \App\Game::STATUS_ACTIVE)
                                        <td class="available" onclick="window.location='/play/{{$row}}/{{$column}}'"></td>
                                    @else
                                        <td class="empty"></td>
                                    @endif
                                @else
                                    @if($cell == \App\Board::VALUE_HIT)
                                        <td class="hit"></td>
                                    @elseif($cell == \App\Board::VALUE_MISS)
                                        <td class="miss"></td>
                                    @elseif(is_numeric($cell) && $game->type == \App\Game::TYPE_ONE_PLAYER)
                                        <td class="boat"></td>
                                    @else
                                        <td class="empty"></td>
                                    @endif

                                @endif
                            @endforeach
                        </tr>
                    @endforeach
                </table>
            </div>
        @endforeach

        <div>
            <button onclick="window.location = '/new/1'">Nieuw spel tegen de computer</button>
        </div>
        <div>
            <button onclick="window.location = '/new/2'">Nieuw spel voor 2 spelers</button>
        </div>
        <div>
            <button onclick="window.location = '/autoplay'">Autoplay!</button>
        </div>

    </body>
</html>