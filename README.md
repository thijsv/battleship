# Battleship

Rudimentary implementation of the game Battleship including a simple AI, done in 4 hours for a time-limited coding challenge

To deploy locally:

- clone the project
- create the database and tables using /database/create-tables.sql
- set the environment variables DB_DATABASE, DB_USERNAME and DB_PASSWORD in the /.env file
- run: composer install
- run: php artisan serve